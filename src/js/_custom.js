(function($) {
    'use strict';

    $(function() {
        var w = window.innerWidth;
        var h = window.innerHeight;


        // Article Items
        var $article    =   $('.article-item');
        $article.each(function() {
            var $this   =   $(this),
                $title  =   $this.find('.article-item-title'),
                $info   =   $this.find('.article-item-productinfo');
                $info.addClass('float-sm-right');

                $title.append($info);
        });


    });

})(jQuery);
($)